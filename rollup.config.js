import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import replace from '@rollup/plugin-replace';
import babel from '@rollup/plugin-babel';
import pkg from './package.json';
import 'postcss-load-config';
import 'postcss-import';

const production = !process.env.ROLLUP_WATCH;
const mode = process.env.NODE_ENV;
const dev = mode === 'development';

export default {
	input: 'src/main.js',
	preserveEntrySignatures: false,
	output: {
		sourcemap: true,
		format: 'esm',
		name: 'app',
		dir: 'public/build',
		chunkFileNames: '[name]-bundle.js'
	},
	plugins: [
		svelte({
			preprocess: sveltePreprocess({ postcss: true }),
			dev: !production,
			css: css => {
				css.write('public/build/bundle.css');
			}
		}),
		babel({
			exclude: "node_modules/**",
			presets: ["@babel/env", "@babel/preset-react"],
			babelHelpers: 'bundled',
			plugins: [
				"@babel/plugin-proposal-class-properties"
				]
		}),
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		replace({
			// 'process.browser': true,
			// 'process.env.NODE_ENV': JSON.stringify(mode)
			'process.env.NODE_ENV': JSON.stringify(production ? 'production' : 'development')
		}),
		commonjs({
			// include: ["node_modules/**"],
			// exclude: ["node_modules/process-es6/**"],			
		}),
		!production && serve(),
		!production && livereload('public'),
		production && terser()
	],
	watch: {
		clearScreen: false
	}
};

function serve() {
	let started = false;

	return {
		writeBundle() {
			if (!started) {
				started = true;

				require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
					stdio: ['ignore', 'inherit', 'inherit'],
					shell: true
				});
			}
		}
	};
}
