import React, {Fragment} from 'react';
import firebase from './firebase';
import SvelteComponent from "./Editor.svelte";
import toReact from "svelte-adapter/react";
const baseStyle = {
	width: "25%"
  };
   
let Editor = toReact(SvelteComponent, baseStyle, "div");

class Opts extends React.Component{
    constructor(props){
        super(props);
        this.state={
            selectedSchemaKey: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.saveSchema = this.saveSchema.bind(this);
    }
    schemasLocal= {};
    schemasLocalArray= [];
    componentDidMount(){
    }

    handleChange(e){
        this.selectedSchema = e.target.value;
        Object.keys(this.props.alldata).forEach(key=>{
            if(this.selectedSchema === key){
                this.selectedSchemaKey = this.props.alldata[key];
                this.setState({selectedSchemaKey : this.selectedSchemaKey});
            }
        })
    }
    handleJson = (event) => {
        this.selectedSchemaKey = event.detail.data;
    }

    saveSchema(data){
        this.props.alldata[this.selectedSchema] = data;
        firebase.database().ref('schemas/'+this.selectedSchema).set(data)
    }

    render() {
        let opx = this.props.options.map((opt)=>{  
             return <option key={opt.key} value={opt.key}>{opt.name}</option>

        })
        let data = this.props.alldata[this.selectedSchema];
        return(
    <div>
            <select onChange={this.handleChange}>
                {opx}
            </select>
            <span>
               <Editor json={this.selectedSchemaKey} onMagicalclick={this.handleJson}/>
            </span>
            <br /><br />
            <button onClick={() => this.saveSchema(this.selectedSchemaKey)}>Save Json</button>
    </div>
        )
    }
}

export default Opts;