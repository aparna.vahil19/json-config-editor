import React, { useState } from "react";
import ReactDOM from "react-dom";
// import SvelteComponent from "./Editor.svelte";
import App from './App.jsx'
// import toReact from "svelte-adapter/react";

// const baseStyle = {
// 	width: "25%"
//   };
   
// const SvelteInReact = toReact(SvelteComponent, baseStyle, "div");

// function App() {
// 	 return(
// 			// <div>
// 			//   <SvelteInReact /><SvelteInReact /><SvelteInReact />
// 			// </div>
// 	 );
// }

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
