import React, { Fragment } from 'react';
import firebase from './firebase';
import schema from './schema'
import Opts from './Opts.jsx';

class Fbase extends React.Component{
    constructor(props){
        super(props);
        this.createSchemas = this.createSchemas.bind(this);
        this.state={
            schemaNameState : [],
            schemasLocalArr : []
        }
    }

    schemasLocal = {};
    schemasLocalArray = [];

    createSchemas(){
        console.log("create schemas");
        firebase.database().ref().set(schema)
        .then(cb => {
        })
    }

    render(){
        return(
            <div className="App-header">
                <span>JSON Editor App</span> 
                <br />
                <button onClick={this.createSchemas}>Set Schemas from file to FB</button>
                <br />
                Existing Schemas
                <Opts options={this.state.schemaNameState} alldata={this.state.schemasLocalArr} />
            </div>
        )
    }

    componentDidMount() {
        let root = firebase.database().ref();
        
        root.on('value', (snapshot)=>{
            this.schemasLocal = {};
            this.schemasLocalArray = [];
            this.schemasLocal = snapshot.val().schemas;
            Object.keys(this.schemasLocal).forEach(key=>{
                this.schemasLocalArray.push(
                    {   "key" : key.toString(), 
                        "name": snapshot.val().schemas[key].name.toString()
                    })
            })
            this.setState({
                schemaNameState : this.schemasLocalArray,
                schemasLocalArr : snapshot.val().schemas
            });
        })
    }
    componentWillUnmount() {  }

}

export default Fbase;